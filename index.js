const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());

let users = [
  {
    userName: "TStark3000",
    email: "starkindustries@mail.com",
    password: "notPeterParker",
  },
  {
    userName: "ThorThunder",
    email: "loveAndThunder@mail.com",
    password: "iLoveStormBreaker",
  },
];

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/greeting", (req, res) => {
  res.send("Hello from Batch244-Almadrones");
});

app.post("/hello", (req, res) => {
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

app.post("/signup", (req, res) => {
  if (
    req.body.username !== "" &&
    req.body.password !== "" &&
    req.body.email !== ""
  ) {
    users.push(req.body);
    res.send(`User ${req.body.userName} is successfully registered`);
  } else {
    res.send("Please input BOTH username and password");
  }
});

app.put("/change-password", (req, res) => {
  let sameUser;
  let user = req.body.userName;
  sameUser = users.find((users) => users.userName === user);
  if (sameUser !== null) {
    res.send("User found!");
  } else {
    res.send("User not found!");
  }
});

// [ITEM 1]
app.get("/home", (req, res) => {
  res.send("Welcome to the homepage.");
});

// [ITEM 3]
app.get("/users", (req, res) => {
  res.send(users);
});

// [Item 5]
app.delete("/delete-user", (req, res) => {
  let user = req.body.userName;
  let index = users.map((item) => item.userName).indexOf(user);
  if (index >= 0) {
    users.splice(index, 1);
    res.send(users);
  } else {
    res.send("User not found!");
  }
});

app.listen(port, () => console.log(`Server is running at port ${port}.`));
